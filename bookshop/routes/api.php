<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('hello','BookController@hello')->name('hello');
Route::post('author','AuthorController@store')->name('store-author');
Route::post('book','BookController@store')->name('store-book');

Route::get("unpublished-author",'AuthorController@unpublished_author');
Route::get("atLeastOnePublication", 'AuthorController@atLeastOnePublication');

