<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $table='books';

    protected $fillable =[
        'name', 'author_id', 'published',
    ];

    public function book_authors(){
        return $this->belongsToMany('App\Author');
    }
}
