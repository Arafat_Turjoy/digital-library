<?php

namespace App\Http\Controllers;
use App\Author;

use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function store(Request $request){
        $author = Author::create([
            "name"=>$request->name,
        ]);

        return response()->json(["author"=>$author]);
    }

    public function unpublished_author(Request $request){
        $authors = Author::doesnthave('books')->get();
        return response()->json(['authors'=>$authors]);        
    }

    public function atLeastOnePublication(Request $request){
        $authors = Author::has('books')->get();
        return response()->json(['authors'=>$authors]);
    }
}
