<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use App\Book;
use http\Env\Response;

class BookController extends Controller
{

    public function hello(Request $request){
        return response()->json(["success"=>"Hello"]);
    }
    public function store(Request $request){
        $data=[
            "name"=>$request['name'],
            'author_id'=>$request['author_id'],
            'published_date'=>$request['published_date']
        ];
        $book = Book::create($data);
        return response()->json(['success'=>200, 'book'=>$book]);
    }

    
}
